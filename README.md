Refresh 0.0.1
=============

Refresh is a small client side javascript made mainly creating web apps. It also handles modules and module loading *kind of*
.It comes with some important modules though:

+ **io** - For handling ajax and jsonp requests
+ **utils** - Like you don't already know.

(**Sorry**, the list has dropped in number so refresh.js can be a little smaller).
It's also has the main components for MVSS(**SS** as in *storage* and *socket*) for building web apps.



Why Do You need it
------------------

Don't know, probably heard about it from one of your guys, you know. It works using ideas
mainly from books read by the main author :). But it works mainly on chrome, so you probably don't need it.

Installing it
-------------

Seriously. Anyways I would suggest putting the script tag at the last child of the `body` element for your own good.
Not that we cant handle `head` scripts anyways.:-/

Examples
--------
#### Registering modules
Use the static mapSource() function  to map aliases to paths.
``` js

Refresh.mapSource({
    my_module : 'js/mod.js'
});

```

#### Registering non-amd modules
Non-amd modules llike jQuery and backbone shouldn't be mapped normally but rather through a `noamd` property
``` js

Refresh.mapSource({
    noamd : {
        jQuery : 'js/jquery-1.10.2.js',
        validate : {
          path : 'js/jquery.validate.js',
          required : ['jQuery']
        }
    }
});

```


#### Using modules
It actually loads the modules only when they are to be used(lazy right).It kind of helps with initial load time, *kind of*. All 
modules requested for are passed to the *safe zone* as an aargument to the callback.
``` js

Refresh.run(function (R) {
    var Model = K.Model,
    Template = K.Template;
    
    var Friend = Model.create();
    Friend.attributes = ['name', 'email'];
    
    var f1 = Friend.init({
        name : 'Arewa',
        email : 'danceasarxx@gmail.com'
    });
    
    f1.save();
    
    var friendTmpl = new Template('friend');
    var t = friendTmpl.compile(f1.attributes());
    console.log(t);
    
}, ['utils', 'jQuery', 'validate', 'template']);

```

##### Note :
> If you'll want to use some extensions of the library like **template**, **local** or **view** you'll need to 
download the [modules.json](https://github.com/danceasarxx/refresh) from the github repo. Then point to it(it'll 
actually load it) :
``` js
Refresh.setModuleMap('js/lib/module.json');
```

#### Creating modules
``` js

Refresh.make('convert', function (K) {
    var mod = {};
    K.forEach([1, 2, 4], function (num) {
        console.log(num);
    });
    
    return mod;
}, ['utils']]);

```
