var Refresh = {};

(function (R, window, document) {
    
    'use strict';
    
    var
        // all refresh modules
        modules,
        // mapping of all sources
        sources,
        // list of loaded external scripts
        externals,
        // shortcut to utils module as it is used extensively
        U,
        // Multi event Event system
        Queue,
        // Classes for script management
        External, Script;
        
    
    modules = {};
    sources = {};
    externals = [];
    
    R.Global = {};
    
    function test(bool) {
        return typeof bool !== 'undefined';
    }
    
    
    // normalize ajax request
    if (!window.XMLHttpRequest) {
        window.XMLHttpRequest = function () {
            try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); }
                catch (e1) {
                    try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
                        catch (e2) { return; }
                }
        };
    }

    
    
    R.support = {
        file : test(window.File && window.FileList && window.FileReader),
        'form-data' : test(window.FormData),
        xhr : test(window.XMLHttpRequest || window.ActiveXObject),
        xhr2 : test(window.XMLHttpRequest.prototype.timeout),
        json : test(window.JSON)
    };
    
    
    
    /**
    * All Utility functions used mainly by this library.It includes
    * functions for arrays, objects and some other 'utility'.
    */
    modules.utils = (function (win, dom) {
        
        var mod = {};
        
        mod.error = function (msg, namespace) {
            throw ('Refresh: ' + (namespace || 'Global') + ' - '+ msg);
        };
        
        mod.isArray = function (arr) {
            return arr && arr.constructor === Array;
        };
        
        mod.toArray = function (array) {
            return Array.prototype.slice.call(array, 0);
        };
        
        mod.isObject = function (obj) {
            var key;
            if (!obj || typeof obj !== 'object' || obj.nodeType || obj !== obj.window) {
                return false;
            }

            try {
                // Not own constructor property must be Object
                if (obj.constructor && !(obj.hasOwnProperty('constructor'))
                        && !(obj.constructor.prototype.hasOwnProperty('isPrototypeOf'))) {
                    return false;
                }
            } catch (e) {
                // IE8,9 Will throw exceptions on certain host objects #9897
                return false;
            }

            for (key in obj) {}
            return key === undefined || obj.hasOwnProperty(key);
        };
        
        mod.inherit = function (p) {
            if (Object.create) {
                return Object.create(p);
            }

            function Dummy() {}
            Dummy.prototype = p;
            return new Dummy();
        };
        
        mod.initialize = function (obj, def) {
            var name;
            for (name in def) {
                if (obj[name] === undefined) {
                    obj[name] = p[name];
                }
            }
        };
        
        mod.assert = function (a, msg) {
            if (!a) {
                msg = msg || 'An assertion has fialed';
                this.error(msg, 'AssertionError');
            }
        };
        
        mod.log = function () {
            try {
                console.log.apply(console, arguments);
            } catch (e) {
                alert(Array.prototype.join.call(arguments, " "));
            }
        };
        
        mod.classOf = function (object) {
            if (object === null) {
                return 'Null';
            } else if (object === undefined) {
                return 'Undefined';
            } else if (object.className) {
                return object.className;
            } else {
                return Object.prototype.call(object).slice(8, -1);
            }
        };
        
        mod._switch = function (def, opt) {
            def = String(def);
            var args = Array.prototype.slice.call(arguments, 2);
            if (opt[def]) {
                return opt[def](args);
            } else if (opt._default) {
                return opt._default(args);
            }
        };
        
        mod.guid = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (t) {
                // create an hex randomm number and floor it
                var x = Math.random() * 16 | 0,
                    // if 'y' limit range of results to 8 and (8 + 3)
                    k = t === 'x' ? x : (x & 3 | 8);
                    // return hexadecimal format
                return k.toString(16);
            }).toUpperCase();
        };
        
        function serializeArray(value) {
            var str = mod.bind(mod, mod.stringify);
            return '[' + mod.map(value, str).join(', ') + ']';
        }
        
        function serializeString(value) {
            var special = {
                    '\"': '\\\"',
                    '\\': '\\\\',
                    '\f': '\\f',
                    '\b': '\\b',
                    '\n': '\\n',
                    '\t': '\\t',
                    '\r': '\\r',
                    '\v': '\\v'
                },
                
                escaped = value.replace(/[\"\\\f\b\n\t\r\v]/g, function (c) {
                    return special[c];
                });
            
            return '"' + escaped + '"';
        }
        
        function serializeObject(value) {
            var properties = [];
            value.forEach(function (name, value) {
                properties.push(serializeString(name) + ': ' + mod.stringify(value));
            });
            return '{' + properties.join(',') + '}';
        }
        
        mod.stringify = win.JSON.stringify || function (value) {
            if (this.isArray(value)) {
                return serializeArray(value);
            } else if (typeof value === 'string') {
                return serializeString(value);
            } else if (this.isObject(value)) {
                return serializeObject(value);
            } else {
                return String(value);
            }
        };
        
        mod.parse = win.JSON.parse || function (json) {
            if (json === '') { json = 'null'; }
            eval('var  p = ' + json + ';');
            return p;
        };
        
        
        
        function safeWrap(cons, name, fn) {
            cons.prototype[name] = cons.prototype[name] || fn;
        }
        
        
        /*************Array Patches************/
        
        safeWrap(Array, 'indexOf', function (val) {
            var i;
            for (i = 0; i < this.length; i++) {
                if (this[i] === val) {
                    return i;
                }
            }
            return -1;
        });
        
        safeWrap(Array, 'forEach', function (callback, st, en) {
            st = st || 0;
            en = en || this.length;
            var i;
            for (i = st; i < en; i++) {
                callback(this[i], i);
            }
        });
        
        safeWrap(Array, 'contains', function (element) {
            return this.indexOf(element) !== -1;
        });
        
        safeWrap(Array, 'filter', function (callback) {
            var result = [];
            this.forEach(function (a, i) {
                if (callback(a, i)) {
                    result.push(a);
                }
            });
            return result;
        });
        
        safeWrap(Array, 'merge', function () {
            return this;
        });
        
        safeWrap(Array, 'map', function (callback) {
            var result = [], val;
            this.forEach(function (a, i) {
                val = callback(a, i);
                if (val !== undefined) {
                    result.push(val);
                }
            });
            return result;
        });
        
        
        /***********Object Patches**************/
        
        safeWrap(Object, 'forEach', function (callback) {
            var key, val;
            for (key in this) {
                if (this.hasOwnProperty(key)) {
                    val = callback(key, this[key]);
                    if (val === false) {
                        break;
                    }
                }
            }
        });
        
        
        safeWrap(Object, 'merge', function (src, deep) {
            var name, value;
            for (name in src) {
                value = src[name];
                
                if (deep) {
                    // clone object
                    if (mod.isArray(value)) {
                        this[name] = this.merge([], value, deep);
                    } else if (mod.isObject(value)) {
                        this[name] = this.merge({}, value, deep);
                    } else {
                        this[name] = value;
                    }
                } else {
                    this[name] = value;
                }
            }
        });
        
        safeWrap(Object, 'keys', function () {
            var result = [];
            this.forEach(function (name) { result.push(name); });
            return result;
        });
        
        
        /************Function Patches***************/
        
        safeWrap(Function, 'partial', function () {
            var args = mod.toArray(arguments), fn = this;
            return function () {
                return fn.apply(null, args.concat(mod.toArray(arguments)));
            };
        });
        
        safeWrap(Function, 'bind', function (object) {
            var self = this;
            return function () {
                return self.apply(object, arguments);
            };
        });
        
        return mod;
    }(window, document));
    
    // store refrence to utils since it's used throughout
    U = modules.utils;
    
    
    
    /**
    * Base class and creator of classes.Note that it creates classes
    * not instances of a class it's used in the creation of every other class
    * including model.
    */
    R.Class = function (parent) {
        
        // proxy constructor/initializer
        var klass = function () {
            this.init.apply(this, arguments);
        };
        
        
        // subclassing
        if (parent) {
            klass.prototype = U.inherit(parent.prototype);
            klass.merge(parent);
        }
        
        
        // set default
        klass.prototype.init = function () {};
        
        // shorter method
        klass.fn = klass.prototype;
        
        // shortcut to superclass
        klass.fn.parent = (parent && parent.prototype) || R.Class;
        
        // adding static properties
        klass.extend = function (obj) {
            var extended = obj.extended;
            klass.merge(obj);
            
            // call custom extension message
            if (extended) {
                extended(klass);
            }
        };
        
        klass.implement = function (obj) {
            this.include(obj.prototype);
        };
        
        // adding instance properties
        klass.include = function (obj) {
            var included = obj.included;
            klass.fn.merge(obj);
            
            // call custom inclusion message
            if (included) {
                included(klass);
            }
        };
        
        return klass;
    };
    
    
    
    
    /**
    * This is more like the client side database.It handles creation
    * update, saving and editing of records amongst other things.Usage
    * on both subclasses and their instances.
    */
    R.Model = (function () {
        
        var mod = new R.Class();
        
        mod.extend({
            inherited : function () {},
            
            created : function () {
                // create new record for new model
                this.records = {};
                // record of serializable attributes
                this.attributes = [];
            },
            
            // creates a new model
            create : function () {
                // inherit static methods
                var object =  U.inherit(this);
                object.parent =  this;
                // inherits from model prototype
                object.prototype = object.fn = U.inherit(this.prototype);

                // call special methods
                object.created();
                this.inherited(object);
                return object;
            },
            
            // creates a new instance of this model
            init : function () {
                var instance = U.inherit(this.prototype);
                instance.parent = this;
                instance.init.apply(instance, arguments);
                return instance;
            },
            
            // Searched each model's records for the record with the given id
            find : function (id) {
                var record = this.records[id];
                if (!record) {
                    U.error('Unknown Error', 'Undefined Error');
                }
                // return duplicate so no changes are made till save() is called
                return record.dup();
            },

            // populates the records
            populate : function (values) {
                // reset records
                this.records = {};
                var record, loop;

                values.forEach(function (rec) {
                    // initialise record
                    record = this.init(rec);
                    // save
                    record.create();
                }.bind(this));
            }
            
        });
        
        
        mod.include({
            
            // indicates wether or not a this instance has been saved
            newRecord : true,
            
            init : function (attrs) {
                if (attrs) {
                    this.merge(attrs);
                }
            },

            // adds the instance to its parents records for the first time
            create : function () {
                this.id = this.id || U.guid();
                this.newRecord = false;
                this.parent.records[this.id] = this.dup();
            },

            // removes the instance from the records
            destroy : function () {
                delete this.parent.records[this.id];
            },

            // updates the records using the present instance
            update : function () {
                this.parent.records[this.id] = this.dup();
            },

            // duplicates the entire record
            dup : function () {
                return {}.merge(this, true);
            },

            // convenience function to correctly deal with creating or updating
            save : function () {
                var temp = this.newRecord ? this.create() : this.update();
            },

            // returns a serializable version of this record
            attributes : function () {
                var result = {};

                this.parent.attributes.forEach(function (name) {
                    result[name] = this[name];
                }.bind(this));

                result.id = this.id;
                return result;
            },

            // for stringifying
            toJSON : function () {
                return this.attributes();
            }
        });
        
        return mod;
    }());
    
    
    
    /**
    * Connects the view, the model and the storage classes together.It's the last S in 
    * MVS.It's basically a channel were events can be echoed to.
    */
    R.Socket = (function () {
        var mod = new R.Class();
        
        mod.include({
            _callbacks : {},
            
            emit : function (name, data) {
                var list = this._callbacks[name];
                
                // if the requested event hasn't been subscribed to return
                if (!list) {
                    return this;
                }
                
                // call the handlers with this has the socket
                list.forEach(function (l) {
                    l.apply(this, data);
                }.bind(this));
            },
            
            on : function (name, handler) {
                var calls = this._callbacks;
                // create an event queue if it doesn't exist and push the handler to it
                (calls[name] || (calls[name] = [])).push(handler);
                return this;
            }
        });
        
        return mod;
    }());
    
    
    
    /**
    * Module for Ajax and JSONP transport. Uses simple interface methods
    * while hiding internals like async module.
    */
    modules.io = (function (win, dom) {
        
        var mod = {},
            // Mainly an event handler for now.
            Request = new R.Class(R.Socket),
            Encoder = new R.Class();
        
        Request.include({
            init : function (xhr) {
                this.xhr = xhr;
            }
        });
        
        
        Encoder.extend({
            
            encode : function (data, type) {
                if (this[type]) {
                    return this[type];
                }
                return data;
            },
            
            json : function (data) {
                return U.stringify(data);
            },
            
            'form-data' : function (data) {
                // return falsy if no data of it's not supported
                if (!data && !R.support["form-data"]) {
                    return;
                }
                
                var form = new FormData();
                
                // append all data to the form
                data.forEach(function (key, value) {
                    // make sure it is not a method
                    if (typeof value === 'function') { return; }
                    form.append(key, value);
                });

                return form;
            },
            
            encoded : function (data) {
                if (!data) {
                    return;
                }
                
                var pairs = [];
                data.forEach(function (key, value) {
                    // make sure it is not a method
                    if (typeof value === 'function') {
                        return;
                    }

                    // reformat
                    key = encodeURIComponent(key.replace(' ', '+'));
                    value = encodeURIComponent(value.toString().replace(' ', '+'));
                                               
                    pairs.push(key + '=' + value);
                });

                // return full encoded data
                return pairs.join('&');
            }
        });

        
        function getContentType(type) {
            type = type || 'text';
            
            // supported content type
            var types = {
                text : 'text/plain',
                json : 'application/json',
                xml : 'application/xml; charset=UTF-8',
                html : 'text/html',
                encoded : 'application/x-www-form-urlencoded',
                'form-data' : 'multipart/form-data'
            };
            
            return types[type] || type;
        }
        
        
        
        function getResponse(req) {
            var type = req.headers['Content-Type'], xhr = req.xhr;

            if (type.indexOf('xml') !== -1 && xhr.responseXML) {
                return xhr.responseXML;
            } else if (/^text/.test(type)) {
                return xhr.responseText;
            } else if (type === 'application/json' && JSON) {
                return U.parse(xhr.responseText);
            } else {
                return xhr.response;
            }
        }
        
        
        var stateHandlers = {
            '0' : function (req, e) {
                req.emit('abort', { evt : e });
            },

            '1' : function (req, e) {
                req.emit('open', { evt : e });
            },

            '2' : function (req, e) {
                req.emit('header', { evt : e });
            },

            '3' : function (req, e) {
                req.emit('loading', { evt : e });
            },

            '4' : function (req, e) {
                req.emit('complete', { evt : e });
                if (req.xhr.status === 200) {
                    req.emit('success', {
                        evt : e,
                        response : getResponse(req)
                    });
                } else {
                    req.emit('error', {
                        evt : e,
                        status : req.xhr.status
                    });
                }
            }
        };
        
        
        function setupHandlers(req, args) {
            var xhr = req.xhr;
            //  set timeout for request
            if (args.timeout > 0) {
                // if xhr2 is supported
                if (xhr.timeout && xhr.ontimeout) {
                    
                    xhr.timeout = args.timeout;
                    xhr.ontimeout = function (e) {
                        req.emit('timeout', { evt : e });
                    };
                } else {
                    // timeout immitation
                    setTimeout(function () {
                        // call timeout after aborting so we don't get into any trouble from timeout()
                        xhr.abort();
                        req.emit('timeout', {});
                    }, args.timeout);
                }
            }

            // set progress events
            xhr.onloadstart = function (e) {
                req.emit('loadstart', { evt : e });
            };

            xhr.onloadend = function (e) {
                req.emit('loadend', { evt : e });
            };

            // register state handler
            xhr.onreadystatechange = function (e) {
                var state = xhr.readyState;
                U._switch(state, stateHandlers, req, e);
            };
        }
        
        
        function setHeaders(req, headers) {
            var xhr = req.xhr;
            headers.forEach(function (name, value) {
                xhr.setRequestHeader(name, value);
            });
        }
        
        
        
        function normalizeGet(arg) {
            arg = U.initialize(arg, {
                query : {},
                nocache : true,
                timeout : 0
            });
            
            arg.nocache ? arg.query._ = (new Date()).getTime()
                        : arg.query;
            
            arg.url += '?' + Encoder.encoded(arg.query);
            arg.headers = arg.headers || {};
            return arg;
        }
        
        
        function normalizePost(arg) {
            arg = U.initialize(arg, {
                query : {},
                timeout : 0
            });
            
            arg.query = Encoder.encode(arg.query, arg.type);
            arg.headers = arg.headers || {};
            // copy type to headers
            arg.headers['Content-type'] = getContentType(arg.type);
            return arg;
        }
        
        
        mod.get = function (url, callback) {
            // get the arguments first
            var args = typeof url === 'string'
                ? {
                    url : url,
                    async : true
                }
                : url,
                req = new Request(new XMLHttpRequest());
             
            // normalize the args
            args = normalizeGet(args);
            
            // set up default event listeners
            setupHandlers(req, args);
            if (callback) {
                req.on('complete', callback);
            }
            
            req.xhr.open('GET', args.url);
            setHeaders(req, args.headers);
            
            req.xhr.send();
            return req;
        };
        
        
        mod.post = function (url, data, callback) {
            // get the arguments first
            var args = typeof url === 'string'
                ? {
                    url : url,
                    query : data,
                    async : true
                }
                : url,
                req = new Request(new XMLHttpRequest());
             
            // normalize the args
            args = normalizePost(args);
            
            // set up default event listeners
            setupHandlers(req, args);
            if (callback) {
                req.on('complete', callback);
            }
            
            req.xhr.open('POST', args.url, args.async);
            setHeaders(req, args.headers);
            
            req.xhr.send(args.query);
            return req;
        };
  
        
        mod.jsonp = function (url, callback, data, name) {
            // identifier
            var id  = name || ('jsonp' + mod.jsonp.counter++),
                cbname = 'Refresh.Global.' + id,
                scr = dom.createElement('script');
            
            // reset data
            data = data || {};
            
            // append callback name
            data.jsonp = cbname;
            // create the query
            url += '?' + Encoder.encoded(data);
            
            // link to Refresh.Global
            R.Global[id] = function (d) {
                try {
                    callback(U.parse(d));
                } finally {
                    // clean up
                    delete R.Global[id];
                    scr.parentNode.removeChild(scr);
                }
            };
            
            // load the data
            scr.src = url;
            dom.body.appendChild(scr);
        };
        
        mod.jsonp.counter = 0;

        
        return mod;
    }(window, document));
    
    
    
    Queue = new R.Class();
    Queue.extend(R.Socket.fn);
    
    // Creates a handler that decides the right moment to call
    // a listener.
    R.Global['queue:handler'] = function (fn) {
        return function () {
            // if we are ready to listen
            if (fn.ready) {
                fn.events--;
                if (fn.events === 0) {
                    fn();
                }
            } else {
                // increase the queue
                ++fn.queue;
            }
        };
    };
    
    

    Queue.extend({
        
        published : [],
        
        queue : function (name, fn) {
            if (this.published.contains(name)) {
                // create and call handler
                R.Global["queue:handler"](fn)();
            }
            
            // increase number of events if we have set events
            fn.events = fn.events !== undefined ? ++fn.events : 1;

            // initialise the ready flag
            fn.ready = fn.ready === undefined ? false : fn.ready;

            // also initialize queue
            fn.queue = fn.queue || 0;
            
            // create an handler and subscribe
            this.on(name, R.Global["queue:handler"](fn));
        },
        
        publish : function (name) {
            var list = this._callbacks[name];
            if (list) {
                this.published.push(name);
            }
            this.emit(name);
        },

        closeDeal : function (fn) {
            if (fn.ready !== undefined) {
                fn.ready = true;
                fn.events -= fn.queue;
                if (fn.events === 0) {
                    fn();
                }
            }
        }
    });
        
        
    
    function loadScript(scr, callback) {
        var code = document.createElement('script');
        callback = callback || function () {};
        code.onload = function () {
            callback();
            document.body.removeChild(code);
        };
        code.src = scr.path;
        document.body.appendChild(code);
    }
        
        
    
    function decorate(arr) {
        var Dummy = function () {},
            fns = ['mapSource', 'run', 'make'];
        
        Dummy.fn = Dummy.prototype;
        
        // copy Refresh top priority classes only
        R.forEach(function (name, fn) {
            if (fns.contains(name)) {
                return;
            }
            Dummy.fn[name] = fn;
        });
        
        arr.forEach(function (name) {
            // don't bother if it's an external script
            if (externals.contains(name)) {
                return;
            }
            
            // attach to dummy prototype
            Dummy.fn[name] = modules[name];
        });
        
        // return new instance so as enforce a kind of encapsulation
        return new Dummy();
    }
    
    
    
    Script = new R.Class();
    
    Script.include({
        init : function (name, path) {
            this.name = name;
            this.path = path;
            this.pending = false;
        },

        load : function () {
            if (!this.pending) {
                this.pending = true;
                loadScript(this);
            }
        }
    });
        

    
    External = new R.Class(Script);

    External.include({
        init : function (name, path, required) {
            this.parent.init.call(this, name, path);
            this.required = required;
            this.loaded = false;
        },

        load : function () {
            if (this.pending || this.loaded) {
                return;
            }
            
            this.pending = true;
            
            
            var self = this, handler =  function () {
                // load script after dependencies have loaded
                // and then publish when this has loaded.
                loadScript(self, function () {
                    self.loaded = true;
                    externals.push(self.name);
                    Queue.publish(self.name);
                });
            };
            
            // load required scripts
            if (this.required) {
                // load all dependencies.
                this.required.forEach(function (a) {
                    var src =  sources[a];
                    if (src) {
                        // subscribe for each dependency's load event
                        Queue.queue(src.name, handler);
                        // load the script
                        src.load();
                    } else {
                        // no such module
                        U.error(a + ' has nor been registered', 'Load Error');
                    }
                });
                
                // start listening
                Queue.closeDeal(handler);
            } else {
                handler();
            }
        }
    });
    
    
    /**
    * Registers scripts to be used by modules.Can also register modules that
    * are not of refresh
    */
    R.mapSource = function (map) {
        if (map) {
            if (map.noamd) {
                map.noamd.forEach(function (name, path) {
                    // don't overwrite
                    if (sources.hasOwnProperty(name)) {
                        return;
                    }
                    
                    // object values are used when there are dependencies
                    if (typeof path === 'object') {
                        sources[name] = new External(name, path.path, path.required);
                    } else {
                        sources[name] = new External(name, path);
                    }
                });
                // so we don't iterate over it again
                delete map.noamd;
            }
            
            map.forEach(function (name, path) {
                // don't overwrite
                if (sources.hasOwnProperty(name)) {
                    return;
                }
                sources[name] = new Script(name, path);
            });
        }
    };
    
    
    
    R.run = function (callback, arr) {
        if (arr) {
            var
                handler = function () {
                    // decorate enviroment with modules
                    var env = decorate(arr);
                    callback(env);
                },
                async = false;
            
            
            arr.forEach(function (nm) {
                // loaded modules and external scripts
                if (modules.hasOwnProperty(nm) || externals.contains(nm)) {
                    return;
                } else {
                    var mod = sources[nm];
                    if (mod) {
                        async = true;
                        // subscribe to module's event and load it.
                        Queue.queue(nm, handler);
                        mod.load();
                    } else {
                        U.error(nm + ' was not found', 'Module Error');
                    }
                }
            });
            
            // start listening
            Queue.closeDeal(handler);
            
            // call immediately if all modules have been loaded
            if (!async) {
                handler();
            }
            
        } else {
            callback({});
        }
    };
    
    
    
    R.make = function (name, callback, arr) {
        if (arr) {
            var
                // handler to be called when all modules have been loaded
                handler = function () {
                    // decorate enviroment with modules
                    var env = decorate(arr);

                    // create module and store it.Then publish the event.
                    modules[name] = callback(env);
                    Queue.publish(name);
                },
                // indicates that there's a need for asynchronous module loading
                async = false;
            
            
            arr.forEach(function (nm) {
                // loaded modules and external scripts
                if (modules.hasOwnProperty(nm) || externals.contains(nm)) {
                    return;
                } else {
                    var mod = sources[nm];
                    if (mod) {
                        async = true;
                        // subscribe to module's event and load it.
                        Queue.queue(nm, handler);
                        mod.load();
                    } else {
                        U.error(nm + ' was not found', 'Module Error');
                    }
                }
            });
            
            // start listening
            Queue.closeDeal(handler);
            
            // call immediately if all modules have been loaded
            if (!async) {
                handler();
            }
            
        } else {
            // just add the module and publish it's event
            modules[name] = callback({});
            Queue.publish(name);
        }
    };
    
    R.setModuleMap = function (path) {
        modules.io.get({
            
        });
    };
    
}(Refresh, window, document));

