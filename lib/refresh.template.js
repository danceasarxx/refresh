Refresh.make('template', function (R) {
    'use strict';
    var
        // Extracts data we are to interpolate
        encloser = /<%(.+?)%>/g,
        // logic that can be run
        // note only helpere is supported
        runnable = /var|if|for|else|switch|case|break|\{|\}|;|helper/g,
        // holds the code that'll be called in the end
        code,
        // tells us our current location in the input
        cursor,
        Template;
    
    
    function add(line, run) {
        run ? (code += line.match(runnable) ? line + '\n' : 'r.push(' + line + ');\n')
            : (code += line !== '' ? 'r.push(\'' + line.replace(/"/g, '\\"') + '\');\n' : '');
        return add;
    }
    
    
    function reset() {
        code = 'with(obj){\nvar r = [];\n';
        cursor = 0;
    }
    
    
    function innerCompile(text, obj) {
        reset();
        var result;
        
        while ((result = encloser.exec(text))) {
            // add top line and code
            add(text.slice(cursor, result.index))(result[1], true);
            cursor = result.index + result[0].length;
        }
        
        // add the remaining
        add(text.substr(cursor, text.length - cursor));
        
        // finish up the code
        code = (code + 'return r.join("");\n}').replace(/[\r\t\n]/g, ' ');
        try {
            result = new Function('obj', code).apply(obj, [obj]);
        } catch (e) {
            console.error('Refresh::Code Error: An error occurred : ' + e.name + ' -- ' + e.message);
            console.error('Code::' + code);
        }
        
        return result;
    }
    
    
    Template = new R.Class();
    
    Template.extend({
       
        compile : function (id, obj) {
            var node = document.getElementById(id), text;
            if (node) {
                text = node.innerHTML;
                return innerCompile(text, obj);
            }
            R.utils.error('Can\'t find the node with ID"' + id + '"', 'Node Error');
        }
    });
    
    Template.include({
        init : function (id) {
            var node = document.getElementById(id);
            if (node) {
                this.text = node.innerHTML;
            } else {
                R.utils.error('Can\'t find the node with ID"' + id + '"', 'Node Error');
            }
        },
        
        compile : function (data) {
            return innerCompile(this.text, data);
        }
    });
    
    return {
        Template : Template
    };
    
}, ['utils']);