Refresh.make('test', function (R) {
    
    'use strict';
    
    var mod = {},
        // holds all the test suites to run
        queue = [],
        // indicates the current state of the test-suite, use in
        // asynchronous operations
        paused = false,
        dom = document,
        // holds all the results of the tests
        testResults,
        // list of assertions per case
        caseNode;
    
    testResults = dom.getElementById('results');
    
    
    function runQueue() {
        // Respond only when not paused
        if (!paused && queue.length) {
            queue.shift()();
            
            // if there were no async operations
            if (!paused) {
                mod.resume();
            }
        }
    }
    
    function $$(name) {
        return dom.createElement(name);
    }
    
    
    mod.assert = function (val, msg) {
        var li = $$('li');
        li.className = val ? 'pass' : 'fail';
        li.appendChild(dom.createTextNode(msg));
        if (!caseNode) {
            caseNode = $$('ul');
            testResults.appendChild(caseNode);
        }
        caseNode.appendChild(li);
    };
    
    mod.pause = function () {
        paused = true;
    };
    
    mod.resume = function () {
        paused = false;
        // deque
        setTimeout(runQueue, 1);
    };
    
    mod.test = function (obj) {
        // queue up functions to log the results
        obj.each(function (name, fn) {
            queue.push(function () {
                var node = $$('div'), head = $$('h3');
                node.className = 'inst-case';
                head.className = 'inst-head';

                head.appendChild(dom.createTextNode(name));
                node.appendChild(head);

                caseNode = $$('ul');

                node.appendChild(caseNode);
                testResults.appendChild(node);

                fn();
            });
        });
        runQueue();
    };
    
    mod.assertEquals = function (one, two, msg) {
        mod.assert(one == two, msg || one + ' and ' + two + ' are not equal');
    };
    
    mod.assertSame = function (one, two, msg) {
        mod.assert(one === two, msg || one + ' and ' + two + ' are not equal');
    };
    
    
    return mod;
}, ['utils']);