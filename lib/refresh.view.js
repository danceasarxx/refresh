Refresh.make('view', function (R) {
    
    var mod = {},
        View = new R.Class(),
        Views =  new R.Class();
    
    View.include({
        init : function (id) {
            
        }
    })
    
    mod.View = View;
    mod.Views = Views;
    
    return mod;
}, ['utils', 'template', 'jQuery']);