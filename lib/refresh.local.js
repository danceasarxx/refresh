Refresh.make('local', function (R) {
    'use strict';
    var mod = {},
        U = R.utils,
        dom = document;

    function getScope(scope) {
        if (scope === 'session') {
            return sessionStorage;
        }
        return localStorage;
    }

    mod.get = function (name, scope) {
        var value = getScope(scope).getItem(name);

        // parse if in JSON format.
        return (/^(\{\[).*(\}\])$/).test(value) ? U.parse(value) : value;
    };


    mod.set = function (key, value, scope) {
        var serializable = U.isArray(value) || U.isObject(value);
        value = serializable ? U.stringify(value) : value;

        getScope(scope).setItem(key, value);
    };


    mod.setCookie = function (name, value, days) {
        var cookie_string = name + "=" + encodeURIComponent(value);
        if (days) {
            cookie_string += "; max-age=" + (days * 60 * 60 * 24);
        }
        dom.cookie = cookie_string;
    };


    mod.getCookies = function () {
        var cookies = {}, all = dom.cookie, pairs;

        // return empty object if the document has no cookies
        if (all === "") {
            return cookies;
        }

        pairs = all.split(" ;");

        pairs.forEach(function (el) {
            var i = el.indexOf("="),
                name = el.substring(0, i),
                value = el.substring(i + 1);

            value = decodeURIComponent(value);
            cookies[name] = value;
        });
        return cookies;
    };

    return mod;
    
}, ['utils']);