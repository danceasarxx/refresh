Refresh.make('collections', function (R) {
    
    var mod =  {};
    
    var List  = mod.List = new R.Class();
    
    List.include({
        _pos : 0,
        _listSize : 0,
        _dataStore : [],
        
        length : function () {
            return this._listSize;
        },
        
        pos : function () {
            return this._pos;
        },
        
        clear : function () {
            
        },
        
        toString : function () {
            
        },
        
        getElement : function () {
            
        },
        
        insert : function () {
            
        },
        
        move : function () {
            
        },
        
        append : function (data) {
            this._dataStore[this._listSize++] = data;
        },
        
        front : function () {
            
        },
        
        end : function () {
            
        },
        
        remove : function () {
            
        },
        
        prev : function () {
            
        },
        
        next : function () {
            
        },
        
        find : function (data) {
            
        },
        
        contains : function () {
            
        }
    });
    
}, ['utils']);